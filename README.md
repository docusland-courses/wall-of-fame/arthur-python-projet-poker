# python-projet-poker



Par équipe de 3, vous devrez réaliser une petite application permettant de simuler les bases d'un jeu de poker.

## Objectif

- être versionné sur GIT (plateforme de votre choix)
- avoir un fichier README avec la description du projet et des fonctionnalités mises en place
- chaque membre de votre équipe doit avoir commité
- avoir un fichier requirements.txt


Au vu du temps imparti, les règles du poker seront ignorées. Voici les règles qui doivent être prises en compte au sein de votre application. Plus vous avancez mieux c'est mais privilégiez la qualité à la quantité.

## Obligatoire :

- Un joueur jouant face à 3 IA.
- génération des cartes et distribution aléatoire (2 cartes par joueur, 3 cartes sur la table)
- avoir une représentation graphique du jeu. (TK ou autre)
- Il doit être possible de saisir un seed (graine), ce qui génèrera la même répartition des cartes.

## Bonus :

- Comparaison des combinaisons (paires)
- Respect des règles du texas holdem
    - un joueur commence avec 10 €
    - La petite bind est définie à 1€, la grosse blind à 2€.
    - Les IA acceptent automatiquement les enchères.

## Pré-requis :

- Python 3.9

## Installation :

- Installer pipenv pour gérer les dépendances et l'environnement virtuel en même temps:
  - ```pip install pipenv```
- lancer le shell de l'environnement virtuel:
  - ``pipenv shell``
- installer les dépendances dans l'environnement virtuel:
  - ``pipenv install --dev``

## Dépendances installées:

- Pytest pour les tests
- Coverage pour la couverture des tests
- Pylint pour la propreté du code
- Numpy pour la gestion des aléatoires

## Développement:
- Faire une branche en local:
  - ``git checkout -b ma-branche-de-dev``
- Vérifier la "propreté" du code:
  - ``pylint *.py models/ tests/ controllers/``
- Lancer les tests (qu'il faudra écrire) et générer la couverture:
  - `` coverage run --branch --source controllers,models,main -m pytest -v``
- Vérifier la couverture des tests et en rajouter si nécessaire (objectif: >=80%): 
  - ``coverage html``
  - Dans un navigateur, ouvrir le fichier: ``htmlcov/index.html``
- Faire les commits sur cette branche
- Une fois que les développements sont prêts à être envoyés sur le repo distant:
  - ``git push origin ma-branche-de-dev``
- Puis faire une merge request sur Gitlab si le pipeline passe, sinon faire les corrections nécessaires.