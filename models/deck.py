"""Module containing the Deck model."""
import numpy as np

from models import Card


class Deck:
    """Represents a deck of playing card with its own seed for pseudo-random shuffling."""

    CARD_SUITS = ['Clovers', 'Diamonds', 'Hearts', 'Spades']
    CARD_RANKS_AND_VALUES = [
        ('2', 2),
        ('3', 3),
        ('4', 4),
        ('5', 5),
        ('6', 6),
        ('7', 7),
        ('8', 8),
        ('9', 9),
        ('10', 10),
        ('Jack', 11),
        ('Queen', 12),
        ('King', 13),
        ('Ace', 14),
    ]

    def __init__(self, seed=0):
        """
        Represents a deck of playing card with its own seed for pseudo-random shuffling.

        :param seed: the seed used to generate a pseudo-random shuffle
        """
        self.seed = seed
        self.cards = []
        for suit in self.CARD_SUITS:
            for rank, value in self.CARD_RANKS_AND_VALUES:
                self.cards.append(Card(rank=rank, value=value, suit=suit))

    def __len__(self):
        return len(self.cards)

    def __eq__(self, other):
        return self.cards == other.cards

    def shuffle_cards(self):
        """
        Method that handles the cards shuffling with the deck's seed.

        :return: the shuffled deck.
        """
        np.random.seed(self.seed)
        np.random.shuffle(self.cards)
        return self

    def deal_card(self) -> Card:
        """
        Method that handles the card dealing.

        :return: the dealt card
        """
        return self.cards.pop()
