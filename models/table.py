"""Module containing the Table model."""
from models import Card


class Table:
    """Represents the poker table where cards are revealed or burned"""
    NUMBER_OF_CARDS_IN_FLOP = 3

    def __init__(self):
        self.burner = []
        self.community_cards = []

    def reveal_card(self, card: Card):
        """
        Method that allows to reveal a card on the table.

        :param card: the card to be revealed.
        :return: the poker table
        """
        card.is_face_up = True
        self.community_cards.append(card)
        return self

    def burn_card(self, card: Card):
        """
        Method that allows to put aside, or 'burn' a card before revealing the next one.

        :param card: the card to be burned.
        :return: the poker table
        """
        self.burner.append(card)
        return self
