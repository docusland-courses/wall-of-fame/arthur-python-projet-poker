"""Application tests."""
import unittest


class PokerAppTest(unittest.TestCase):
    """Class that handles all the app's unit tests."""

    def test_placeholder(self):
        """placeholder test for project scaffold"""
        placeholder = 'placeholder'
        self.assertEqual('placeholder', placeholder)
